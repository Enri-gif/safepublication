package com.example.safepublication;

public class Calc implements Runnable{
    private int num;
    private int result;
    public Calc(int Num){
        num = Num;
    }
    public int GetResult(){
        return result;
    }

    @Override
    public void run() {
        result = num*num;
    }
}
