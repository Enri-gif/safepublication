package com.example.safepublication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.LinkedList;
import java.util.Queue;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Calc c = new Calc(5);
        new Thread(c).start();

        //lave en kø
        Queue<Integer> q = new LinkedList<>();
        //enqueue med en tråd
        q.add(c.GetResult());
        //dequeue med en anden tråd
        Calc c1 = new Calc(q.remove());
        new Thread(c1).start();
    }
}